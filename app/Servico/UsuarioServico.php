<?php

namespace App\Servico;

use App\Dtos\UsuarioDTO;
use App\Repositorio\UsuarioRepositorio;
use App\Utils\ValidaDados;
use Exception;
use Illuminate\Http\Request;

class UsuarioServico
{
    private $usuarioRepositorio;

    public function __construct(UsuarioRepositorio $usuarioRepositorio)
    {
        $this->usuarioRepositorio = $usuarioRepositorio;
    }

    public function cadastrarUsuario(Request $requisicao) {

        try {
            ValidaDados::validarDadosUsuario($requisicao, false);
            $usuarioDTOParaCadastro = new UsuarioDTO();
            $usuarioDTOParaCadastro->nome = $requisicao->nome;
            $usuarioDTOParaCadastro->login = $requisicao->login;
            $usuarioDTOParaCadastro->senha = md5($requisicao->senha);
            $usuarioDTOParaCadastro->ativo = $requisicao->ativo;
            $usuarioDTOParaCadastro->nivelDeAcesso = $requisicao->nivelDeAcesso;
            $usuarioCadastrado = $this->usuarioRepositorio->salvar($usuarioDTOParaCadastro);

            return response()->json($usuarioCadastrado, 201);
        } catch (Exception $e) {
            // escrever no arquivo log.txt o erro que ocorreu!


            return response()->json(
                'Ocorreu o seguinte erro: ' . $e->getMessage(),
                500
            );
        }

    }

    public function buscarUsuarioPeloId($id) {

    }

    public function buscarTodosUsuarios() {

    }

    public function editarUsuario(Request $requisicao) {

    }
}