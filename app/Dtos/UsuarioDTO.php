<?php

namespace App\Dtos;

class UsuarioDTO
{
    public $id;
    public $nome;
    public $login;
    public $senha;
    public $ativo;
    public $nivelDeAcesso;

    public function __construct()
    {
        $this->id = 0;
        $this->nome = '';
        $this->login = '';
        $this->senha = '';
        $this->ativo = true;
        $this->nivelDeAcesso = 0;
    }
}