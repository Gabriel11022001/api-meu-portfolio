<?php

namespace App\Repositorio;

use App\Dtos\UsuarioDTO;
use App\Models\Usuario;

class UsuarioRepositorio implements IRepositorio
{

    public function salvar($entidade) {
        $usuario = new Usuario();
        $usuario->nome = $entidade->nome;
        $usuario->login = $entidade->login;
        $usuario->senha = $entidade->senha;
        $usuario->ativo = $entidade->ativo;
        $usuario->nivelDeAcesso = $entidade->nivelDeAcesso;

        if ($usuario->save() === false) {

        }

        $entidade->id = $usuario->id;

        return $entidade;
    }

    public function buscarPeloId($id) {
        $usuario = Usuario::find($id);

        if ($usuario === null) {
            
        }

        $usuarioDTO = new UsuarioDTO();
        $usuarioDTO->id = $usuario->id;
        $usuarioDTO->nome = $usuario->nome;
        $usuarioDTO->login = $usuario->login;
        $usuarioDTO->senha = $usuario->senha;
        $usuarioDTO->ativo = $usuario->ativo;
        $usuarioDTO->nivelDeAcesso = $usuario->nivelDeAcesso;

        return $usuarioDTO;
    }

    public function buscarTodos() {
        
        return Usuario::all()->toArray();
    }

    public function editar($entidade) {

    }
}
