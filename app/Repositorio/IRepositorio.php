<?php

namespace App\Repositorio;

interface IRepositorio
{

    public function salvar($entidade);

    public function buscarTodos();

    public function buscarPeloId($id);

    public function editar($entidade);
}