<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id();
            $table->string('nome')
                ->nullable(false);
            $table->string('login')
                ->nullable(false)
                ->unique(true);
            $table->string('senha')
                ->nullable(false);
            $table->boolean('ativo')
                ->nullable(false)
                ->default(true);
            $table->integer('nivelDeAcesso')
                ->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
};
